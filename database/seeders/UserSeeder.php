<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use DB, Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Correo predeterminado
        DB::table('users')->insert([
            'role' => '1',
            'name' => 'Prueba',
            'lastname' => 'uno',
            'email' => 'prueba@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
    }
}
